package com.kapilsony.excel2pdf;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.geometry.Pos;
import javafx.geometry.Insets;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
public  class ProgressForm {
        private final Stage dialogStage;
        private final ProgressBar pb = new ProgressBar();
        private final ProgressIndicator pin = new ProgressIndicator();
        private final Label percentLabel=new Label("QRCode Processed Count: 0");
        private final Label licenseMsg=new Label("License Detail: ");

        public ProgressForm() {
            dialogStage = new Stage();
            dialogStage.initStyle(StageStyle.UTILITY);
            dialogStage.setResizable(true);
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.setMinWidth(300);
            // PROGRESS BAR
            final Label label = new Label();
            label.setText("alerto");

            pb.setProgress(-1F);
            pin.setProgress(-1F);

            final VBox hb = new VBox();
            hb.setSpacing(5);
            hb.setPadding(new Insets(10));
            hb.setAlignment(Pos.CENTER);
            hb.getChildren().addAll(pin,pb,percentLabel,licenseMsg);

            Scene scene = new Scene(hb);
            dialogStage.setScene(scene);
        }

        public void activateProgressBar(final Task<?> task)  {
            pb.progressProperty().bind(task.progressProperty());
            pin.progressProperty().bind(task.progressProperty());
            percentLabel.textProperty().bind(task.messageProperty());
            dialogStage.show();
        }
        
        public void setProgressCount(String number) {
        	percentLabel.setText("QRCode Processed Count: "+number);
        }
        
        public void setLicenseMsg(String msg) {
        	licenseMsg.setText(msg);
        }

        public Stage getDialogStage() {
            return dialogStage;
        }
    }