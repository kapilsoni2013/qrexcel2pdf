package com.kapilsony.excel2pdf;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.kapilsony.excel2pdf.ExcelConfig.ExcelRow;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;  
import javafx.stage.FileChooser;
import javafx.concurrent.Task;
public class App extends Application {
	static ExecutorService threadPool = Executors.newFixedThreadPool(1000);
	private String errorMsg="";

	@SuppressWarnings({ "restriction", "unchecked" })
	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
        primaryStage.setTitle("Excel2Pdf Generator");
        
        BorderPane bp = new BorderPane();
        bp.setPadding(new Insets(10,50,50,50));
         
        //Adding HBox
        HBox hb = new HBox();
        hb.setPadding(new Insets(20,20,20,30));
         
        //Adding GridPane
        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(20,20,20,20));
        gridPane.setHgap(5);
        gridPane.setVgap(5);
         
       //Implementing Nodes for GridPane
        Label lblUserName = new Label("FolderName");
        final TextField txtUserName = new TextField("testFolder");
        Label lblPassword = new Label("Excel File");
        TextField chooseLocationField = new TextField("choose file");
        FileChooser fileChooser = new FileChooser();
//        fileChooser.setInitialDirectory(new File("C:\\Users\\Prime Developer\\workshop-sts\\excel2pdf"));
        chooseLocationField.setOnMouseClicked(e -> {
            try {
            	File selectedFile = fileChooser.showOpenDialog(primaryStage);
                excelLocation=selectedFile.getPath();
                chooseLocationField.setText(excelLocation);
            }catch(Exception ex) {}
        });

        Button btnLogin = new Button("Create PDF");
        final Label lblMessage = new Label();
         
        //Adding Nodes to GridPane layout
        gridPane.add(lblUserName, 0, 0);
        gridPane.add(txtUserName, 1, 0);
        gridPane.add(lblPassword, 0, 1);
        gridPane.add(chooseLocationField, 1, 1);
        gridPane.add(btnLogin, 2, 1);
        gridPane.add(lblMessage, 1, 2);
         
                 
        //Reflection for gridPane
        Reflection r = new Reflection();
        r.setFraction(0.7f);
        gridPane.setEffect(r);
         
        //DropShadow effect 
        DropShadow dropShadow = new DropShadow();
        dropShadow.setOffsetX(5);
        dropShadow.setOffsetY(5);
         
        //Adding text and DropShadow effect to it
        Text text = new Text("Pdf Generator");
        text.setFont(javafx.scene.text.Font.font("Courier New", FontWeight.BOLD, 28));
        text.setEffect(dropShadow);
         
        //Adding text to HBox
        hb.getChildren().add(text);
                           
        //Add ID's to Nodes
        bp.setId("bp");
        gridPane.setId("root");
        btnLogin.setId("btnLogin");
        text.setId("text");
                 
        //Action for btnLogin
		btnLogin.setOnAction(new EventHandler() {
			@Override
			public void handle(Event event) {
				if(txtUserName.getText().toString().isEmpty()) {
					lblMessage.setText("Please Enter FolderName");
					lblMessage.setTextFill(javafx.scene.paint.Color.RED);
					return;
				}
				if(chooseLocationField.getText().toString().isEmpty()) {
					lblMessage.setText("Please Choose an Excel file");
					lblMessage.setTextFill(javafx.scene.paint.Color.RED);
					return;
				}
				
				if(!new File(chooseLocationField.getText().toString()).exists()) {
					lblMessage.setText("Please Choose an valid Excel file");
					lblMessage.setTextFill(javafx.scene.paint.Color.RED);
					return;
				}
				
				folderName=txtUserName.getText().toString();
				ProgressForm pForm = new ProgressForm();
				try {
					URL url=new URL("http://kapilsony.com/verify.php");
					HttpURLConnection conn=(HttpURLConnection)url.openConnection();
					BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
					String s=br.readLine();
					SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
					Date date=sdf.parse(s.split("###")[2]);
					date.setHours(23);
					date.setMinutes(59);
					date.setSeconds(59);
					if(s.split("###")[0].equals("0") || date.before(new Date())) {
						lblMessage.setText("License valid till: "+s.split("###")[2]);
						lblMessage.setTextFill(javafx.scene.paint.Color.RED);
						return;
					}
					pForm.setLicenseMsg("License valid till: "+s.split("###")[2]);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					lblMessage.setText("Internet Issue");
					lblMessage.setTextFill(javafx.scene.paint.Color.RED);
					e1.printStackTrace();
					return;
				}
				
				// In real life this task would do something useful and return 
	            // some meaningful result:
				CustomAsyncTask<Void> task = new CustomAsyncTask<Void>() {
	                @Override
	                public Void call() throws InterruptedException {
	            		try {
	            			
	            			long start = System.currentTimeMillis();
		            		createDirectories(folderName);
		            		System.out.println("111111");
							double totalRowsCount=generatePdfFiles(this,size, 10, 7,excelLocation);
							updateProgress(100, 100);
							updateMessage("QRCode Processed Count: "+totalRowsCount);
							System.out.println("222222");
							System.out.println("Welcome");
		            		long end = System.currentTimeMillis();
		            		System.out.println("DONE: " + (end - start));
		            		this.succeeded();
						} catch (Throwable e) {
							this.failed();
							e.printStackTrace();
							errorMsg=e.getMessage();
							 lblMessage.setText("Exception: "+errorMsg);
					         lblMessage.setTextFill(javafx.scene.paint.Color.RED);
						}
	            		
	                    return null ;
	                }
	                @Override
	                public void updateMessage(String message) {
	                	// TODO Auto-generated method stub
	                	super.updateMessage(message);
	                }
	                
	                @Override
	                public void updateProgress(double workDone, double max) {
	                	// TODO Auto-generated method stub
	                	super.updateProgress(workDone, max);
	                }
	            };

	            // binds progress of progress bars to progress of task:
	            pForm.activateProgressBar(task);

	            // in real life this method would get the result of the task
	            // and update the UI based on its value:
	            task.setOnSucceeded(seccEvent -> {
	                pForm.getDialogStage().close();
	                btnLogin.setDisable(false);
	                lblMessage.setText("PDF File generated Successfully");
		            lblMessage.setTextFill(javafx.scene.paint.Color.GREEN);
	            });
	            
	            task.setOnCancelled(canEvent->{
	            	 pForm.getDialogStage().close();
		             btnLogin.setDisable(false);
		             lblMessage.setText("Process Cancelled by User");
		             lblMessage.setTextFill(javafx.scene.paint.Color.RED);
	            });
	            task.setOnFailed(failEvent->{
	            	 pForm.getDialogStage().close();
		             btnLogin.setDisable(false);
		             lblMessage.setText("Exception: "+errorMsg);
			         lblMessage.setTextFill(javafx.scene.paint.Color.RED);
	            });
	            pForm.getDialogStage().setOnCloseRequest(winEvt->{
	            	task.cancel(true);
	            	 pForm.getDialogStage().close();
		             btnLogin.setDisable(false);
		             lblMessage.setText("Process Cancelled by User");
		             lblMessage.setTextFill(javafx.scene.paint.Color.RED);
	            });
	            btnLogin.setDisable(true);
	            pForm.getDialogStage().show();
	            new Thread(task).start();
	            System.out.println(task.isRunning());
				
			}
		});
        
        //Add HBox and GridPane layout to BorderPane Layout
        bp.setTop(hb);
        bp.setCenter(gridPane);  
         
        //Adding BorderPane to the scene and loading CSS
     Scene scene = new Scene(bp);
     scene.getStylesheets().add(getClass().getClassLoader().getResource("login.css").toExternalForm());
     primaryStage.setScene(scene);
       primaryStage.titleProperty().bind(
                 scene.widthProperty().asString().
                 concat(" : ").
                 concat(scene.heightProperty().asString()));
     //primaryStage.setResizable(false);
        primaryStage.show();  
	}
	public static void main(String[] args)
			throws WriterException, IOException, URISyntaxException, DocumentException, InterruptedException {
		launch(args);
//		long start = System.currentTimeMillis();
//		createDirectories(folderName);
//		generatePdfFiles(size, 10, 7);
//		System.out.println("Welcome");
//		long end = System.currentTimeMillis();
//		System.out.println("DONE: " + (end - start));
		
//		long start = System.currentTimeMillis();
//		createDirectories(folderName);
//		System.out.println("111111");
//		double totalRowsCount=generatePdfFiles(null,size, 10, 7,excelLocation);
//		System.out.println("222222");
//		System.out.println("Welcome");
//		long end = System.currentTimeMillis();
//		System.out.println("DONE: " + (end - start));

	}

	private static String folderName = "pdfs";
	private static String excelLocation = "C:\\Users\\Prime Developer\\workshop-sts\\excel2pdf\\abc.xlsx";
	private static int size = 60;

	private static void createDirectories(String folderName) {
		File directoryFile = new File(folderName);
		if (!directoryFile.exists()) {
			directoryFile.mkdir();
		}

		/*
		 * for (int k = 1; k <=10; k++) { new
		 * File(folderName+File.separator+subFolderName+ k).mkdir(); }
		 */
	}

	static Document document = null;

	private static double generatePdfFiles(CustomAsyncTask<Void> task,int size, final int numRows, final int numColumns, String excelLocation2)
			throws WriterException, IOException, URISyntaxException, DocumentException {

		ExcelConfig excelConfig = ExcelConfig.getInstance(excelLocation,0);
		ArrayBlockingQueue<List<List<ExcelRow>>> blockingQueue = new ArrayBlockingQueue<List<List<ExcelRow>>>(1);
		double totalRowsCount=0;
		try {
			totalRowsCount=excelConfig.getTotalRowsCount();
			excelConfig.getRowsBatch(numRows, numColumns, blockingQueue);
			int[] subFolderCount = new int[1];
			subFolderCount[0] = 1;
			document = PdfConfig.getDocument(folderName + File.separator + "File_" + subFolderCount[0] + ".pdf");
			int qrCodeCount=0;
			while (true) {
				List<List<ExcelConfig.ExcelRow>> item = blockingQueue.take();
				System.out.println(item);
				int totalQrCode = item.stream().mapToInt(i -> i.size()).sum();
				qrCodeCount+=totalQrCode;
				List<ExcelRow> lastRow = item.get(item.size() - 1);
				ExcelRow lastQrCode = lastRow.get(lastRow.size() - 1);

				Integer lastQrSerialNo = Integer.parseInt(lastQrCode.serialNo);
				// Updating Progress Bar
				task.updateProgress((double)lastQrSerialNo, totalRowsCount);
				task.updateMessage("QRCode Processed Count: "+lastQrSerialNo);
				
				// double batchNumber=Math.ceil(100000/(numRows*numColumns));
				System.out.println("lastQrSerialNo>> " + lastQrSerialNo);
				
				createQRImage(totalQrCode,document, item, size, numRows, numColumns);
				if (lastQrSerialNo.intValue() >= 100000) {
					System.out.println("==============");
					System.out.println(lastQrSerialNo+"===");
					System.out.println((Integer.parseInt(lastQrCode.serialNo.charAt(0) + "") + 1)+"===");
					System.out.println(subFolderCount[0]+"===");
					System.out.println("==============");
				}
				
				if (lastQrSerialNo.intValue() >= 100000 
						&& Integer.parseInt(lastQrCode.serialNo.charAt(0) + "") + 1 > subFolderCount[0]) {
					System.out.println("NEXT FILE CREATING===========");
					subFolderCount[0] = Integer.parseInt(lastQrCode.serialNo.charAt(0) + "") + 1;
					document.close();
					document = PdfConfig
							.getDocument(folderName + File.separator + "File_" + subFolderCount[0] + ".pdf");
				}
				if (lastQrCode.isLast) {
					if (document.isOpen()) {
						document.close();
					}
					System.out.println("This is Last Row : " + lastRow.get(lastRow.size() - 1).serialNo);
					break;
				}
			}
//			threadPool.shutdown();
//
//			while (!threadPool.isTerminated()) {
//
//			}
			
			System.out.println("Data Extraction Finished");
			return totalRowsCount;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return totalRowsCount;
	}

	private static void createQRImage(int totalQrCode,Document document, List<List<ExcelRow>> item, int size,
			final int numRows, final int numColumns)
			throws WriterException, IOException, URISyntaxException, DocumentException, InterruptedException {

//		int totalQrCode = item.stream().mapToInt(i -> i.size()).sum();
		System.out.println("totalQrCode>>> " + totalQrCode);
//		ExecutorService threadPool = Executors.newFixedThreadPool(totalQrCode);
		CountDownLatch countDownLatch = new CountDownLatch(totalQrCode);

		// Create the ByteMatrix for the QR-Code that encodes the given String
		Map<EncodeHintType, Object> hintMap = new HashMap();
		hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
		// hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");
		hintMap.put(EncodeHintType.MARGIN, 0); /* default = 4 */
		final QRCodeWriter qrCodeWriter = new QRCodeWriter();
		for (int i = 0; i < item.size(); i++) {
			final List<ExcelRow> excelRowList = item.get(i);
			for (int j = 0; j < excelRowList.size(); j++) {
				final int[] jCounter = new int[] { j };
				threadPool.execute(new Runnable() {
					public void run() {
						try {
							
							// TODO Auto-generated method stub
							ExcelRow excelRow = excelRowList.get(jCounter[0]);
							BitMatrix byteMatrix = qrCodeWriter.encode(excelRow.qrString, BarcodeFormat.QR_CODE, size,
									size, hintMap);
							// Make the BufferedImage that are to hold the QRCode
							int matrixWidth = byteMatrix.getWidth();
							BufferedImage image = new BufferedImage(matrixWidth, matrixWidth,
									BufferedImage.TYPE_USHORT_GRAY);

							image.createGraphics();
							Graphics2D graphics = (Graphics2D) image.getGraphics();
							graphics.setColor(Color.WHITE);
							graphics.fillRect(0, 0, matrixWidth, matrixWidth);
							// Paint and save the image using the ByteMatrix
							graphics.setColor(Color.BLACK);

							for (int ii = 0; ii < matrixWidth; ii++) {
								for (int jj = 0; jj < matrixWidth; jj++) {
									if (byteMatrix.get(ii, jj)) {
										graphics.fillRect(ii, jj, 1, 1);
									}
								}
							}
							excelRow.img = Image.getInstance(image, null);
							countDownLatch.countDown();
						} catch (Throwable t) {
							t.printStackTrace();
						}
					}
				});

			}
		}
		countDownLatch.await();
//		threadPool.shutdown();
		// ImageIO.write(image, fileType, qrFile);
		createPdfByImage(document, item, numRows, numColumns);
	}

	private static void createPdfByImage(Document document, List<List<ExcelRow>> item, final int numRows,
			final int numColumns) throws URISyntaxException, DocumentException, MalformedURLException, IOException {
		PdfPTable table = new PdfPTable(numColumns);
		System.out.println("Cteate#### "+item.size());
		for (int i = 0; i < item.size(); i++) {
			final List<ExcelRow> excelRowList = item.get(i);
			System.out.println("excelRowList@@@@ "+excelRowList.size());
			for (int j = 0; j < excelRowList.size(); j++) {
				table.addCell(getCustomCell(excelRowList.get(j)));
			}
		}
		document.add(table);
		document.newPage();
	}

	private static PdfPCell getCustomCell(ExcelRow excelRow) throws URISyntaxException, BadElementException, IOException {
		PdfPCell imageCell = new PdfPCell();
		imageCell.setPadding(5f);
		imageCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		imageCell.addElement(excelRow.img);
		Paragraph p1 = new Paragraph(excelRow.serialNo);
		p1.setFont(FontFactory.getFont(FontFactory.COURIER_BOLD, 6, BaseColor.BLACK));
		p1.setAlignment(Element.ALIGN_CENTER);
		imageCell.addElement(p1);
		return imageCell;
	}

}
