package com.kapilsony.excel2pdf;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;

public class PdfConfig {

	public static Document getDocument(String fileName) throws FileNotFoundException, DocumentException {
		Document document = new Document();
		document.setMargins(10,10,10,10);
		PdfWriter.getInstance(document, new FileOutputStream(fileName));
		document.open();
		return document;
	}
}
