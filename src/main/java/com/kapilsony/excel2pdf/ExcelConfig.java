package com.kapilsony.excel2pdf;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.itextpdf.text.Image;
import com.monitorjbl.xlsx.StreamingReader;

public class ExcelConfig {
	private Workbook workbook = null;
	private String excelLocation="";
	private Sheet sheet=null;

	public ExcelConfig(String excelLocation,int sheetNum) throws FileNotFoundException {
		this.excelLocation=excelLocation;
		sheet=getSheetAt(sheetNum);
	}

	public static ExcelConfig getInstance(String excelLocation,int sheetNum) throws FileNotFoundException {
		return new ExcelConfig(excelLocation,sheetNum);
	}

	public Workbook getWorkbook() throws FileNotFoundException {
		if (workbook == null) {
			InputStream is = new FileInputStream(new File(excelLocation));
			workbook = StreamingReader.builder()
					.rowCacheSize(40000) // number of rows to keep in memory // (defaults to 10)
					.bufferSize(20000) // buffer size to use when reading InputStream to file (defaults to 1024)
					.open(is); // InputStream or File for XLSX file (required)
		}
		return workbook;
	}

	public Sheet getSheetAt(int sheetNum) throws FileNotFoundException {
		workbook = getWorkbook();
		sheet=workbook.getSheetAt(sheetNum);
		return sheet;

	}
	
	public String getSheetName() {
		return sheet==null?"Error Sheet Not Loaded":sheet.getSheetName();
	}
	
	public double getTotalRowsCount() {
		return sheet==null?0:sheet.getLastRowNum();
	}

	/**
	 * Get Excel Rows in Batch
	 * @param sheetNum Excel Sheet Number
	 * @param numRows Number of Rows Require in PDF
	 * @param numColumns Number of Columns Require in PDF
	 * @param blockingQueue Blocking Queue to Send Data back to Consumer when numRows set in Batch List
	 * @throws InterruptedException
	 * @throws FileNotFoundException
	 */
	public void getRowsBatch(final int numRows,final int numColumns,final ArrayBlockingQueue<List<List<ExcelRow>>> blockingQueue) throws InterruptedException, FileNotFoundException {
		
		new Thread() {
			@Override
			public void run() {
				try {
					System.out.println(sheet.getSheetName()+" : "+sheet.getLastRowNum());
					List<List<ExcelRow>> recordsList=new ArrayList<List<ExcelRow>>();
					List<ExcelRow> record=new ArrayList<ExcelConfig.ExcelRow>();
					
					for (Row r : sheet) {
//						System.out.println("NNNN: "+r.getRowNum());
							ExcelRow excelRow=new ExcelRow(r.getCell(0).getStringCellValue(),r.getCell(1).getStringCellValue());
							if(r.getRowNum()==sheet.getLastRowNum()) {
								excelRow.isLast=true;
							}
							record.add(excelRow);
							
							if(record.size()==numColumns) {
								recordsList.add(record);
								record=new ArrayList<ExcelConfig.ExcelRow>();	
							}
							if(recordsList.size()==numRows) {
								blockingQueue.put(recordsList);
								recordsList=new ArrayList<List<ExcelRow>>();
							}
					}
					if(!record.isEmpty() && record.get(record.size()-1).isLast) {
						recordsList.add(record);
						blockingQueue.put(recordsList);
					}
					/*for(int row=0;row<sheet.getLastRowNum();row++) {
						Row r=sheet.getRow(row);
						
						ExcelRow excelRow=new ExcelRow(r.getCell(0).getStringCellValue(),r.getCell(1).getStringCellValue());
						if(row==sheet.getLastRowNum()-1) {
							excelRow.isLast=true;
						}
						record.add(excelRow);
						
						if(record.size()==numColumns) {
							recordsList.add(record);
							record=new ArrayList<ExcelConfig.ExcelRow>();	
						}
						if(recordsList.size()==numRows) {
							blockingQueue.put(recordsList);
							recordsList=new ArrayList<List<ExcelRow>>();
						}
					}*/
				}catch(Throwable t) {
					t.printStackTrace();
				}
			};
		}.start();
		
		
	}

	public static class ExcelRow{
		public String serialNo;
		public String qrString;
		public Image img;
		public boolean isLast=false;
		
		public ExcelRow(String serialNo, String qrString) {
			this.serialNo = serialNo;
			this.qrString = qrString;
		}
		@Override
		public String toString() {
			return "{\"serialNo\":"+serialNo+",\"qrString\":"+qrString+"}";//"ExcelRow [serialNo=" + serialNo + ", qrString=" + qrString + "]";
		}
		
		
	}
}
