package com.kapilsony.excel2pdf;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.kapilsony.excel2pdf.ExcelConfig.ExcelRow;
public class GenerateQRCode {

	public static void main(String[] args) throws WriterException, IOException, DocumentException, URISyntaxException, InterruptedException {
		ArrayBlockingQueue<Integer> blockingQueue=new ArrayBlockingQueue<>(1);
		blockingQueue.put(1);
		blockingQueue.put(1);
		blockingQueue.put(1);
		System.out.println("11");
		System.out.println(blockingQueue.take());
		System.out.println(blockingQueue.take());
		System.out.println(blockingQueue.take());
//		String qrCodeText = "https://www.journaldev.com";
//		String filePath = "JD.png";
//		int size = 125;
//		String fileType = "png";
//		File qrFile = new File(filePath);
//		createQRImage(qrFile, qrCodeText, size, fileType);
//		createPdf();
//		long start=System.currentTimeMillis();
//		ExecutorService threadPool = Executors.newFixedThreadPool(5000);
//		for(int i=0;i<1000;i++) {
//			threadPool.execute(new Runnable() {
//				@Override
//				public void run() {
//					try {
//						readExcel();
//						Thread.sleep(50000);
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					} catch (FileNotFoundException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
//			});
//		}
		long end=System.currentTimeMillis();
//		System.out.println("DONE: "+(end-start));
	}
	
	private static void readExcel() throws FileNotFoundException {
		ExcelConfig excelConfig=ExcelConfig.getInstance("",0);
		ArrayBlockingQueue<List<List<ExcelRow>>> blockingQueue=new ArrayBlockingQueue<List<List<ExcelRow>>>(1);
		try {
			excelConfig.getRowsBatch(10, 7, blockingQueue);
			while (true) 
            {
				List<List<ExcelConfig.ExcelRow>> item = blockingQueue.take();
                System.out.println(item);
                List<ExcelRow> lastRow = item.get(item.size()-1);
                if(lastRow.get(lastRow.size()-1).isLast) {
                	System.out.println("This is Last Row : "+lastRow.get(lastRow.size()-1).serialNo);
                	break;
                }
            }
			System.out.println("Data Extraction Finished");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void createPdf() throws DocumentException, URISyntaxException, IOException {
		int numRows=10;
		Document document = new Document();
		document.setMargins(10,10,10,10);
		PdfWriter.getInstance(document, new FileOutputStream("iTextTable.pdf"));
		document.open();
		PdfPTable table = new PdfPTable(7);
		addCustomRow(table,null,"1",numRows,table.getNumberOfColumns());
		document.add(table);
		document.close();

	}


	private static void addCustomRow(PdfPTable table,java.awt.Image buffImage,String qrCodeText,int numRows,int numColumns) throws URISyntaxException, BadElementException, IOException {
		Path path = Paths.get("JD.png");
	    Image img = Image.getInstance(path.toAbsolutePath().toString());
		
		PdfPCell imageCell = new PdfPCell();
		imageCell.setPadding(5f);
		imageCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		imageCell.addElement(img);
		Paragraph p1 = new Paragraph(qrCodeText);
		p1.setFont(FontFactory.getFont(FontFactory.COURIER_BOLD, 6, BaseColor.BLACK));
		p1.setAlignment(Element.ALIGN_CENTER);
		imageCell.addElement(p1);
		
		for(int i=0;i<numRows*numColumns;i++) {
			table.addCell(imageCell);
		}
	}


	private static void createQRImage(File qrFile, String qrCodeText, int size, String fileType)
			throws WriterException, IOException {
		// Create the ByteMatrix for the QR-Code that encodes the given String
		Map<EncodeHintType, Object> hintMap = new HashMap();
		hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
		hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");
		hintMap.put(EncodeHintType.MARGIN, 0); /* default = 4 */
		QRCodeWriter qrCodeWriter = new QRCodeWriter();
		BitMatrix byteMatrix = qrCodeWriter.encode(qrCodeText, BarcodeFormat.QR_CODE, size, size, hintMap);
		// Make the BufferedImage that are to hold the QRCode
		int matrixWidth = byteMatrix.getWidth();
		BufferedImage image = new BufferedImage(matrixWidth, matrixWidth, BufferedImage.TYPE_INT_RGB);
		image.createGraphics();

		Graphics2D graphics = (Graphics2D) image.getGraphics();
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, matrixWidth, matrixWidth);
		// Paint and save the image using the ByteMatrix
		graphics.setColor(Color.BLACK);
		for (int i = 0; i < matrixWidth; i++) {
			for (int j = 0; j < matrixWidth; j++) {
				if (byteMatrix.get(i, j)) {
					graphics.fillRect(i, j, 1, 1);
				}
			}
		}
		ImageIO.write(image, fileType, qrFile);
	}

}