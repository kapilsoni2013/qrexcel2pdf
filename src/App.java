package com.kapilsony.excel2pdf;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.kapilsony.excel2pdf.ExcelConfig.ExcelRow;

public class App {
	static ExecutorService threadPool = Executors.newFixedThreadPool(1);

	public static void main(String[] args)
			throws WriterException, IOException, URISyntaxException, DocumentException, InterruptedException {
		long start = System.currentTimeMillis();
		createDirectories(folderName);
		generatePdfFiles(size, fileType, 10, 7);
		System.out.println("Welcome");
		long end = System.currentTimeMillis();
		System.out.println("DONE: " + (end - start));

	}

	private static String subFolderName="SET-";
	private static String folderName = "pdfs";
	private static int size = 60;
	private static String fileType = "png";
	private static void createDirectories(String folderName) {
		File directoryFile = new File(folderName);
		if (!directoryFile.exists()) {
			directoryFile.mkdir();
		}

		/*for (int k = 1; k <=10; k++) {
			new File(folderName+File.separator+subFolderName+ k).mkdir();
		}*/
	}

	private static List<List<List<ExcelConfig.ExcelRow>>> iLakhList=null;
	private static void generatePdfFiles(int size, String fileType,
			final int numRows, final int numColumns)
			throws WriterException, IOException, URISyntaxException, DocumentException {

		ExcelConfig excelConfig = ExcelConfig.getInstance();
		ArrayBlockingQueue<List<List<ExcelRow>>> blockingQueue = new ArrayBlockingQueue<List<List<ExcelRow>>>(1);

		try {
			excelConfig.getRowsBatch(0, numRows, numColumns, blockingQueue);
			iLakhList=new ArrayList<List<List<ExcelConfig.ExcelRow>>>();
			int count[]=new int[] {0};
			while (true) {
				List<List<ExcelConfig.ExcelRow>> item = blockingQueue.take();
				List<ExcelRow> lastRow = item.get(item.size() - 1);
				ExcelRow lastQrCode = lastRow.get(lastRow.size() - 1);
				
				System.out.println(item);
				if(iLakhList.size()>=1429) {
					count[0]++;
//					threadPool.execute(new Runnable() {
//						@Override
//						public void run() {
//							try {
//								createQRImage(count[0],iLakhList, size, fileType, numRows, numColumns);
//							} catch (WriterException | IOException | URISyntaxException | DocumentException
//									| InterruptedException e) {
//								e.printStackTrace();
//							}
//						}
//					});
					createQRImage(count[0],iLakhList, size, fileType, numRows, numColumns);
					iLakhList=new ArrayList<List<List<ExcelConfig.ExcelRow>>>();
				}else {
					iLakhList.add(item);
				}
				
				
				
				
				if (lastQrCode.isLast) {
					
					System.out.println("This is Last Row : " + lastRow.get(lastRow.size() - 1).serialNo);
					break;
				}
			}
			if(iLakhList.size()>0) {
				count[0]++;
//				threadPool.execute(new Runnable() {
//					@Override
//					public void run() {
//						try {
//							createQRImage(count[0],iLakhList, size, fileType, numRows, numColumns);
//						} catch (WriterException | IOException | URISyntaxException | DocumentException
//								| InterruptedException e) {
//							e.printStackTrace();
//						}
//					}
//				});
				createQRImage(count[0],iLakhList, size, fileType, numRows, numColumns);
			}
			threadPool.shutdown();
			
			while(!threadPool.isTerminated()) {
				
			}
		
			System.out.println("Data Extraction Finished");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void createQRImage(int count, List<List<List<ExcelConfig.ExcelRow>>> iLackList, int size, String fileType,
			final int numRows, final int numColumns)
			throws WriterException, IOException, URISyntaxException, DocumentException, InterruptedException {
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>  "+iLackList.size());
		Document document = PdfConfig.getDocument(folderName+File.separator+"File_"+count+".pdf");
		
		// Create the ByteMatrix for the QR-Code that encodes the given String
		Map<EncodeHintType, Object> hintMap = new HashMap();
		hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
		// hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");
		hintMap.put(EncodeHintType.MARGIN, 0); /* default = 4 */
		final QRCodeWriter qrCodeWriter = new QRCodeWriter();
		
		for(int k=0;k<iLackList.size();k++) {
			List<List<ExcelConfig.ExcelRow>> item=iLackList.get(k);
			int totalQrCode = item.stream().mapToInt(i -> i.size()).sum();
			for (int i = 0; i < item.size(); i++) {
				final List<ExcelRow> excelRowList = item.get(i);
				System.out.println(excelRowList);
				for (int j = 0; j < excelRowList.size(); j++) {
					final int[] jCounter = new int[] { j };
//					threadPool.execute(new Runnable() {
//						public void run() {
							try {
								// TODO Auto-generated method stub
								ExcelRow excelRow = excelRowList.get(jCounter[0]);
								BitMatrix byteMatrix = qrCodeWriter.encode(excelRow.qrString, BarcodeFormat.QR_CODE, size, size,
										hintMap);
								// Make the BufferedImage that are to hold the QRCode
								int matrixWidth = byteMatrix.getWidth();
								BufferedImage image = new BufferedImage(matrixWidth, matrixWidth, BufferedImage.TYPE_USHORT_GRAY);
								
								image.createGraphics();
								Graphics2D graphics = (Graphics2D) image.getGraphics();
								graphics.setColor(Color.WHITE);
								graphics.fillRect(0, 0, matrixWidth, matrixWidth);
								// Paint and save the image using the ByteMatrix
								graphics.setColor(Color.BLACK);
								
								for (int ii = 0; ii < matrixWidth; ii++) {
									for (int jj = 0; jj < matrixWidth; jj++) {
										if (byteMatrix.get(ii, jj)) {
											graphics.fillRect(ii, jj, 1, 1);
										}
									}
								}
								excelRow.img = Image.getInstance(image, null);
									
								
							}catch(Throwable t) {
								t.printStackTrace();
							}
//						}
//					});

				}
			}
			
			createPdfByImage(document,item, numRows, numColumns);
		}
		document.close();
	}

	private static void createPdfByImage(Document document,List<List<ExcelRow>> item,final int numRows,final int numColumns)
			throws URISyntaxException, DocumentException, MalformedURLException, IOException {
		PdfPTable table = new PdfPTable(numColumns);
		for (int i = 0; i < item.size(); i++) {
			final List<ExcelRow> excelRowList = item.get(i);
			for (int j = 0; j < excelRowList.size(); j++) {
				table.addCell(getCustomCell(excelRowList.get(j)));
			}
		}
		document.add(table);
	}

	private static PdfPCell getCustomCell(ExcelRow excelRow) throws URISyntaxException, BadElementException, IOException {
		PdfPCell imageCell = new PdfPCell();
		imageCell.setPadding(5f);
		imageCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		imageCell.addElement(excelRow.img);
		Paragraph p1 = new Paragraph(excelRow.serialNo);
		p1.setFont(FontFactory.getFont(FontFactory.COURIER_BOLD, 6, BaseColor.BLACK));
		p1.setAlignment(Element.ALIGN_CENTER);
		imageCell.addElement(p1);
		return imageCell;
	}

}
