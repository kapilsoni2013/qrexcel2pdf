package com.kapilsony.excel2pdf;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

public class App {
	static CountDownLatch countDownLatchU = new CountDownLatch(10);
	static CountDownLatch countDownLatchB=null;
	public static void main(String[] args)
			throws WriterException, IOException, URISyntaxException, DocumentException, InterruptedException {
		long start = System.currentTimeMillis();
		for(int k=0;k<10;k++) {
			new File("pdfs"+k).mkdir();
		}
		for (int j = 0; j < 10; j++) {
			countDownLatchU.countDown();
			ExecutorService threadPool = Executors.newFixedThreadPool(250);
			countDownLatchB = new CountDownLatch(100000);
			long startI = System.currentTimeMillis();
			for (int i = 0; i < 100000; i++) {
				
				final int[] ii = new int[2];
				ii[0] = i;
				ii[1] = j;
				threadPool.execute(new Runnable() {
					public void run() {
						countDownLatchB.countDown();
						String folderName = "pdfs";
						String qrCodeText = "https://www.journaldev.com";
						String fileName = "testFile" + ii[1]+"__"+ii[0] + ".pdf";
						int size = 125;
						String fileType = "png";
						try {
							generatePdfFiles(folderName+ii[1], fileName, qrCodeText, size, fileType);
						} catch (WriterException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (URISyntaxException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (DocumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});

			}
			
			countDownLatchB.await();
			long endI = System.currentTimeMillis();
			threadPool.shutdown();
			System.out.println(">>>Step"+j+" Finished: "+ (endI-startI));
		}
		countDownLatchU.await();
		System.out.println("Welcome");
		long end = System.currentTimeMillis();
		System.out.println("DONE: " + (end - start));
		

	}

	private static void generatePdfFiles(String folder, String fileName, String qrCodeText, int size, String fileType)
			throws WriterException, IOException, URISyntaxException, DocumentException {
//		File directoryFile = new File(folder);
//		if (!directoryFile.exists()) {
//			directoryFile.mkdir();
//		}
		createQRImage(folder + File.separator + fileName, qrCodeText, size, fileType);

	}

	private static void createPdfByImage(java.awt.Image buffImage, String fileName)
			throws URISyntaxException, DocumentException, MalformedURLException, IOException {
		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream(fileName));
		document.open();
		Image img = Image.getInstance(buffImage, null);
		document.add(img);
		document.close();
	}

	private static void createQRImage(String fileName, String qrCodeText, int size, String fileType)
			throws WriterException, IOException, URISyntaxException, DocumentException {
		// Create the ByteMatrix for the QR-Code that encodes the given String
		Map<EncodeHintType, ErrorCorrectionLevel> hintMap = new HashMap();
		hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
		QRCodeWriter qrCodeWriter = new QRCodeWriter();
		BitMatrix byteMatrix = qrCodeWriter.encode(qrCodeText, BarcodeFormat.QR_CODE, size, size, hintMap);
		// Make the BufferedImage that are to hold the QRCode
		int matrixWidth = byteMatrix.getWidth();
		BufferedImage image = new BufferedImage(matrixWidth, matrixWidth, BufferedImage.TYPE_INT_RGB);
		image.createGraphics();

		Graphics2D graphics = (Graphics2D) image.getGraphics();
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, matrixWidth, matrixWidth);
		// Paint and save the image using the ByteMatrix
		graphics.setColor(Color.BLACK);

		for (int i = 0; i < matrixWidth; i++) {
			for (int j = 0; j < matrixWidth; j++) {
				if (byteMatrix.get(i, j)) {
					graphics.fillRect(i, j, 1, 1);
				}
			}
		}
		// ImageIO.write(image, fileType, qrFile);
		createPdfByImage(image, fileName);
	}

}
